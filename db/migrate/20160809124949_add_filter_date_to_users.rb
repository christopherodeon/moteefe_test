class AddFilterDateToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :filter_date, :date
  end
end
