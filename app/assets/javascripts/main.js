$(document).on('ready', function(){
	$('#datetimepicker1').datetimepicker({
		inline: true,
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm'
	});
	$('#datetimepicker2').datetimepicker({
		inline: true,
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm'
	});

	$('#datetimepicker3').datetimepicker({
		minView: 2,
		format: 'YYYY-MM-DD'
	});
});