module ApplicationHelper
  def locations_for_select
    Location.all.map{|x| [x.name, x.id]}
  end

  def topics_for_select
    Topic.all.map{|x| [x.name, x.id]}
  end
end
