class User < ApplicationRecord

  include Filtering

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :user_topics, dependent: :destroy
  has_many :topics, through: :user_topics

  belongs_to :location, required: false
end
