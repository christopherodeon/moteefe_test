class Event < ApplicationRecord

  include Filtering

  after_create :notify_users_of_new_event

  belongs_to :location

  has_many :event_topics, dependent: :destroy
  has_many :topics, through: :event_topics

  validates_presence_of :name, :start_date, :end_date

  def notify_users_of_new_event
    User.with_location(location).with_topics(topics).each do |user|
      ApplicationMailer.notify_user_of_new_event(user, self).deliver
    end
  end
end
