module Filtering
  extend ActiveSupport::Concern

  included do
    scope :with_location, ->(location) { 
      where(location_id: location.id) if location
    }
    scope :with_topics, ->(topics) { 
      joins(:topics).where('topics.id in (?)', topics.map(&:id)).uniq if topics.any?
    }
    scope :by_date, ->(date) {
      where('start_date BETWEEN ? AND ?', date.beginning_of_day, date.end_of_day) if date
    }
  end
end