class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  def notify_user_of_new_event(user, event)
    @event = event
    mail(to: user.email, :subject => "A new event has been added")
  end
end
